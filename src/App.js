import { Root } from 'native-base';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import React, { Component } from 'react';
import HomeScreen from './screen/HomeScreen.js';
import NewPost from './screen/NewPost.js';
import Detail from './screen/Detail.js';
import Login from './screen/Login.js';
import YourPets from './screen/YourPets.js';
import Profile from './screen/Profile.js';
import YourBooks from './screen/YourBooks.js';
import AllBooks from './screen/AllBooks.js';
import Camera from './screen/Camera.js';
import MapScreen from './screen/MapScreen.js';
import SideBar from './screen/SideBar'





const Drawer = DrawerNavigator(
  {
    
    HomeScreen: {
      screen: HomeScreen
    },
 
    Profile: {
      screen: Profile
    },
    Login: {
      screen: Login
    },
    YourBooks:{
      screen: YourBooks
    },   
    AllBooks:{
      screen: AllBooks
    }
  },
  {
    initialRouteName: 'AllBooks',
    contentOptions: {
      activeTintColor: '#e91e63',
    },
    contentComponent: props => <SideBar {...props} />
  }
)


const AppNavigator = StackNavigator(

  {
    Drawer: {
      screen: Drawer
    },
    New: {
      screen: NewPost
    },
    Detail:{
      screen: Detail
    },
    Login: {
      screen: Login
    } , Camera:{
      screen: Camera
    },
    MapScreen:{
      screen: MapScreen
    }
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
    cardStyle: {backgroundColor: '#eaebed'},
   
  }
);

export default () => <Root> 
  
  <AppNavigator />

</Root>;
