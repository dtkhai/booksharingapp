import React, { Component } from "react";
import { StyleProvider } from 'native-base';
import App from "../App";
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import { StatusBar } from 'react-native';
import { Provider, observer } from "mobx-react";
import { userStore } from "../stores/index";

@observer
export default class Setup extends Component {
  componentDidMount() {
    StatusBar.setHidden(true);
  }
  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Provider
          userStore={userStore}>
          <App />
        </Provider>
      </StyleProvider>
    );
  }
}
console.ignoredYellowBox = ['Warning:']