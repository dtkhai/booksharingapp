
import React, { Component } from 'react';
import { Alert, Platform, StyleSheet, Text, View, TextInput, KeyboardAvoidingView, AsyncStorage } from 'react-native';
import { Button, CheckBox, Container, Content, Icon } from 'native-base';
import { GoogleSignin } from 'react-native-google-signin';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import firebase from "react-native-firebase";

GoogleSignin.configure({

});


var STORAGE_KEY = 'id_token';

import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";

@inject('userStore')
@observer
class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chkbox_chk: false,
      isLoaded: false,
      email: 'dtkhai@apcs.vn',
      password: '123456',
    };
  }

  componentDidMount() {
    this.autoLogin().then((user) => {
      this.props.userStore.StoreUser(user)
      this.setState({
        isLoaded: true
      })
      this.props.handlerLogin();
    }).catch((error)=>{
      
      this.setState({
        isLoaded: true
      })
    })
  }

  async autoLogin() {
    try {
      var tokenDataString = await AsyncStorage.getItem(STORAGE_KEY);
      var tokenData = JSON.parse(tokenDataString);
      console.info("SAved Token: " + JSON.stringify(tokenData))
      if (tokenDataString) {
        if (tokenData.method == "facebook") {
          const credential = firebase.auth.FacebookAuthProvider.credential(tokenData.tokenValue);
          const currentUser = await firebase.auth().signInAndRetrieveDataWithCredential(credential)
          return (currentUser.user);
        }
        else if (tokenData.method == "google") {
          const credential = firebase.auth.GoogleAuthProvider.credential(tokenData.tokenId, tokenData.tokenValue)
          const currentUser = await firebase.auth().signInAndRetrieveDataWithCredential(credential)
          return (currentUser.user);
        }
      }      
    } catch (error) {
      console.log('Auto loggin error: ' + error);
    }
  }

  ChangeCheck() {
    this.setState({
      chkbox_chk: !this.state.chkbox_chk
    });
  }

  ResetLogin() {

    this.setState({
      email: '',
      password: '',
    });
  }

  googleLogin = async () => {
    try {
      // Add any configuration settings here:
      await GoogleSignin.configure();

      const data = await GoogleSignin.signIn();

      // create a new firebase credential with the token
      const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken)
      this.__saveToken("google", data.accessToken, data.idToken);
      // login with credential
      const currentUser = await firebase.auth().signInAndRetrieveDataWithCredential(credential);

      console.info("Test googleuser:   " + JSON.stringify(currentUser.user.toJSON()));
      this.props.userStore.StoreUser(currentUser.user)
      this.props.handlerLogin();
    } catch (e) {
      console.error(e);
    }
  }

  OnLoginFaceBook = async () => {
    try {
      const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);

      if (result.isCancelled) {
        throw new Error('User cancelled request'); // Handle this however fits the flow of your app
      }

      console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);

      // get the access token
      const data = await AccessToken.getCurrentAccessToken();
      if (!data) {
        throw new Error('Something went wrong obtaining the users access token'); // Handle this however fits the flow of your app
      }
      this.__saveToken("facebook", data.accessToken);
      // create a new firebase credential with the token
      const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);

      // login with credential
      const currentUser = await firebase.auth().signInAndRetrieveDataWithCredential(credential);

      console.info("Creadential: " + JSON.stringify(credential))
      console.info(JSON.stringify(currentUser.user.toJSON()))
      console.info(JSON.stringify(data))

      this.props.userStore.StoreUser(currentUser.user)
      this.props.handlerLogin();
    } catch (e) {
      console.error(e);
    }
  }

  async __saveToken(_method, _tokenValue, _tokenId) {
    try {
      if (_method == "facebook") {
        tokenData = {
          method: _method,
          tokenValue: _tokenValue
        }
      } else {
        tokenData = {
          method: _method,
          tokenValue: _tokenValue,
          tokenId: _tokenId
        }
      }
      await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(tokenData));

      console.info("Here: " + JSON.stringify(tokenData))
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        {this.state.isLoaded &&
          <View>
            <Button full
            iconLeft 
            rounded 
              style={{height:50, backgroundColor: '#0057a5', marginBottom: 15 }}
              onPress={() => this.OnLoginFaceBook()}>
              <Icon type="FontAwesome" name='facebook' style={styles.buttonIcon} />
              <Text style={[styles.buttonText]}>
                Login Facebook
        </Text>
            </Button>
            <Button full
            iconLeft 
            rounded 
              style={{height: 50,backgroundColor: '#ff6d38' }}
              onPress={() => this.googleLogin()}>
              <Icon  type="FontAwesome"  name='google' style={styles.buttonIcon} />
              <Text style={styles.buttonText}>
              Login Google
        </Text>
            </Button>
          </View>
        }
        {!this.state.isLoaded &&
          <View>
            <Text style={styles.h1}>
              LOADING
            </Text>
          </View>
        }

      </KeyboardAvoidingView>


    );
  }
}

const styles = StyleSheet.create({
  h1: {
    textAlign: "center",
    color: "white",
    fontWeight: "bold",
    fontSize: 30,
    textShadowColor: '#555',
    textShadowRadius: 5,
    textShadowOffset: { width: -1, height: 1 },
  },
  inputContainer: {

    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    borderRadius: 30,
    flexDirection: 'row',
    marginBottom: 15,
    alignItems: 'center',
    height: 60,
  },
  rmbText: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10

  },
  container: {
    padding: 20,
    paddingTop: 10
  },
  input: {
    flex: 1,

    borderRadius: 20,
    fontSize: 20,
    marginLeft: 20,
    color: '#062E42',
    borderColor: 'rgb(111,111,111)',
    paddingHorizontal: 10,
  },
  buttonText: {
    color: '#FFF',
    fontWeight: "bold",
    fontSize: 23,
    marginLeft: 10
  },
  buttonIcon: {
    textAlign: "center",
    color: '#FFF',
    fontWeight: "bold",
    fontSize: 30,
    position: 'absolute',
    left: 0
  }
});

export default LoginForm;
