import React, { Component } from 'react';
import { Content, Footer, FooterTab, Button, Icon, Text } from 'native-base';
export default class MyFooter extends Component {
    render() {
        return (
            <Footer style={{position: 'absolute',bottom: 0,zIndex: 10}}>

                <FooterTab>
                    <Button vertical active={this.props.curActive==1} onPress={()=>this.props.handlerNav("Profile")}>
                        <Icon name="person" />
                        <Text>Profile</Text>
                    </Button>
                    <Button vertical active={this.props.curActive==3} onPress={()=>this.props.handlerNav("YourBooks")}>
                        <Icon name="apps" />
                        <Text>My Books</Text>
                    </Button>
                    <Button vertical active={this.props.curActive==4} onPress={()=>this.props.handlerNav("AllBooks")}>
                        <Icon name="camera" />
                        <Text>Explore</Text>
                    </Button>
                </FooterTab>
            </Footer>
        );
    }
}