import React from 'react';
import { StatusBar, Alert, Image, FlatList, TouchableOpacity, BackHandler, ToastAndroid } from 'react-native';
import MapView from 'react-native-maps';
import { Container,Badge, Header, Title, Left, Icon, Right, Button, ListItem, Thumbnail, Form, Item, Label, Input, Body, Content, Text, Card, CardItem, View } from 'native-base';
import firebase from "react-native-firebase";
import Communications from 'react-native-communications';
import MyFooter from '../components/MyFooter'
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

const CATE = [
    "All",
    "General",
    "Economy",    
    "Romantic",
    "Science",
    "Bibiography",
    "Manga",
    "History",
    "Fiction",
    "Other"
]
const DATA = [
    {
        name: "Hay",
        author: "Steve JObs",
        image: "https://pictures.abebooks.com/ARMYDOC/20595056776.jpg",
        username: "KHai",
        description: "This book is about Khai",
        location: {
            latitude: 10.7750247,
            longitude: 106.6978738,
        }
    },
    {
        name: "Ngu",
        author: "Bill GAte JObs",
        image: "https://images-na.ssl-images-amazon.com/images/I/A1HI7mXRymL.jpg",
        username: "No",
        description: "This book is goof",
        location: {
            latitude: 10.762546,
            longitude: 106.680989,
        }
    },
    {
        name: "Best",
        author: "Trump",
        image: "https://upload.wikimedia.org/wikipedia/en/thumb/1/1c/Trump_the_art_of_the_deal.jpg/220px-Trump_the_art_of_the_deal.jpg",
        username: "Trump",
        description: "Make american graete again",
        location: {
            latitude: 10.8454029,
            longitude: 106.7261562,
        }
    },

]

@inject('userStore')
@observer
class AllBooks extends React.Component {
    static navigationOptions = ({ navigation }) => ({
    });

    handlerNav = (e) => this.props.navigation.navigate(e)

    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            data: params ? params.data : null,
            books: [],
            isLoaded: false,
            listBook: [],
            listFiltered: [],
            curTag: "All"
        }
        this.ref = firebase.firestore().collection('Book');

    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        navigator.geolocation.getCurrentPosition((position) => {

            this.setRegion(position.coords)


        }, (error) => {

            alert(JSON.stringify(error) + " Make sure you enable location")
        }, {
                enableHighAccuracy: false,
                timeout: 8000,
                maximumAge: 0
            });
        this.unsubscribe = this.ref.onSnapshot((query) => {
            const tArray = []

            query.forEach((item) => {

                var temp = item.data()
                if (temp.email != this.props.userStore.user.email) {  
                temp.distance = this.getDistance(this.props.userStore.curLoc.latitude, this.props.userStore.curLoc.longitude, temp.location.latitude, temp.location.longitude)
                tArray.push(temp)
                }


            })
            //console.log(JSON.stringify(tArray))
            tArray.sort((a,b)=>{
                return ((a.distance)  - (b.distance));
               })
            this.setState({

                listBook: tArray,
                listFiltered: tArray
            })
        })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }

    setRegion(location) {
        this.props.userStore.setLoc(location.latitude, location.longitude)
        setTimeout(() => this.setState({
            isLoaded: true
        }), 100)

    }

    viewDetail(marker) {
        this.props.navigation.navigate('Detail', {
            data: marker
        })
    }
    renderPet = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => { this.viewDetail(item) }}>
                <ListItem onPress={() => { this.viewDetail(item) }}>

                    <Thumbnail square size={60} source={{ uri: item.image }} />

                    <Body>
                        <Text>{item.name}</Text>
                        <Text note>{item.author}</Text>
                        <Text note>{item.description}</Text>

                    </Body>
                    <Right>
                        <Text>{item.distance} km</Text>
                    </Right>
                </ListItem>
            </TouchableOpacity>
        )
    }

    getDistance(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
            ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        d = d.toFixed(2);
        return d;
    }


    changeTag(tag) {
        this.setState({
            curTag: tag
        });
        if ((tag != "All")) {
            this.setState({
              refreshing: true
            })
            newData = this.state.listBook
            newData = newData.filter((item) => {
                const itemData = item.tag.toUpperCase()
                const textData = tag.toUpperCase()
                return itemData.indexOf(textData) > -1
            });
            this.setState({              
                listFiltered: newData // after filter we are setting users to new array
            });
          
          }
          else {
            this.setState({      
              listFiltered: this.state.listBook
            });
          }
    }

    renderGroupItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => this.changeTag(item)}>
                <Badge style={[{backgroundColor: "#c44b2d", marginLeft: 5, marginTop: 5, marginBottom: 5 }, (this.state.curTag == item) && { backgroundColor: "#3eb27e" }]}>
                    <Text>{item}</Text>
                </Badge>
            </TouchableOpacity>

        );
    }


    filterSearch(text) {
        newData = this.state.listBook
        newData = newData.filter((item) => {
            const itemData = item.name.toUpperCase()
            const textData = text.toUpperCase()
            return itemData.indexOf(textData) > -1
        });
        this.setState({
            text: text,
            listFiltered: newData // after filter we are setting users to new array
        });
    }

    render() {
        return (
            <Container>
                <Header searchBar rounded>
                    <Item>
                        <Icon active name="search" />
                        <Input onChangeText={(text) => this.filterSearch(text)}
                            value={this.state.text} placeholder="Search" />

                    </Item>
                    <Button transparent>
                        <Text>Search</Text>
                    </Button>
                </Header>
                <Content style={{ paddingHorizontal: 5 }} >
                    <FlatList data={CATE}
                        horizontal={true}
                        renderItem={this.renderGroupItem}
                        keyExtractor={(item, index) => index}
                        style={{ margin: 0, padding: 0 }} />
                    {this.state.isLoaded &&
                        <FlatList data={this.state.listFiltered}
                            renderItem={this.renderPet}
                            keyExtractor={(item, index) => index}
                            style={{ margin: 0, padding: 0 }} />
                    }
                    {
                        !this.state.isLoaded && <Text>
                            FINDING NEARBY BOOK...
                            </Text>
                    }


                </Content>
                <MyFooter handlerNav={this.handlerNav} curActive={4} />

            </Container>
        );
    }
}



export default AllBooks;
