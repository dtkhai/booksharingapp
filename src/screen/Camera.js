'use strict';
import React, { Component } from 'react';
import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import { Icon } from 'native-base';
import { RNCamera } from 'react-native-camera';
import { dirPicutures } from '../lib/dirStorage';
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";
const RNFS = require('react-native-fs');
import moment from 'moment';

const moveAttachment = async (filePath, newFilepath) => {
    return new Promise((resolve, reject) => {
      RNFS.mkdir(dirPicutures)
        .then(() => {
          RNFS.moveFile(filePath, newFilepath)
            .then(() => {
              console.log('FILE MOVED', filePath, newFilepath);
              resolve(true);
            })
            .catch(error => {
              console.log('moveFile error', error);
              reject(error);
            });
        }) 
        .catch(err => {
          console.log('mkdir error', err);
          reject(err);
        });
    });
  };


@inject('userStore')
@observer
export default class Camera extends Component {
  render() {
    return (
      <View style={styles.container}>
        <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style = {styles.preview}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.auto}
            permissionDialogTitle={'Permission to use camera'}
            permissionDialogMessage={'We need your permission to use your camera phone'}
        />
        <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center',}}>
        <TouchableOpacity
            onPress={this.takePicture.bind(this)}
            style = {styles.capture}
        >
             <Icon name="camera" style={{ fontSize: 40}} />
        </TouchableOpacity>
        <TouchableOpacity
            onPress={()=>this.props.navigation.goBack()}
            style = {styles.capture}
        >
             <Icon name="md-close" style={{ fontSize: 40}} />
        </TouchableOpacity>
        </View>
      </View>
    );
  }

  takePicture = async function() {
    if (this.camera) {
      const options = { quality: 0.3, base64: true};
      const data = await this.camera.takePictureAsync(options)
      this.props.navigation.navigate('New', {
        image: 'data:image/jpeg;base64,'+data.base64,
        pos: {
          latitude: this.props.userStore.curLoc.latitude,
          longitude: this.props.userStore.curLoc.longitude
        }
    })
      //this.saveImage(data.uri);
      console.log('hinh dayyyyy' + JSON.stringify(data));
      console.log(data.uri);
    }
  };

  saveImage = async filePath => {
    try {
      // set new image name and filepath
      const newImageName = `${moment().format('DDMMYY_HHmmSSS')}.jpg`;
      const newFilepath = `${dirPicutures}/${newImageName}`;
      // move and save image to new filepath
      console.log('OPATH', newFilepath);
      const imageMoved = await moveAttachment(filePath, newFilepath);
      console.log('image moved', imageMoved);
    } catch (error) {
      console.log("THIS ERROOR: " + error);
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20
  }
});

