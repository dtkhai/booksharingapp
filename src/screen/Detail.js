import React from 'react';
import { StatusBar, Alert, Image } from 'react-native';
import MapView from 'react-native-maps';
import { Container, Header, Title, Left, Icon, Right, Button, Form, Item, Label, Input, Body, Content, Text, Card, CardItem, View } from 'native-base';
import firebase from "react-native-firebase";
import Communications from 'react-native-communications';

import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";
@inject('userStore')
@observer
class Detail extends React.Component {
    static navigationOptions = ({ navigation }) => ({
    });

    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            data: params ? params.data : null,

        }
        
    }
    renderBtn(){
        if((this.props.userStore.user.email===this.state.data.email)){
            return(
                <Button full
                    primary
                    success
                    iconLeft                   
                    style={{ margin: 5 }}
                >
                    <Text>
                        DELETE
                </Text>

                </Button>
            )
        }
        else{
            
            return(
                <View>
                      <Button full
                    primary
                    info
                    rounded
                    iconLeft
                    onPress={() =>  this.props.navigation.navigate('MapScreen', {
                        des: this.state.data
                    })}
                    style={{ margin: 5,height:50 }}
                >
                    <Text>
                        Find Direction
                </Text>

                </Button>
               
                <Button full
                    primary
                    rounded
                    iconRight 
                    onPress={() => Communications.email([this.state.data.email],null,null,'Hi','I want to exchange')}
                    style={{ margin: 5,height:50  }}
                >
                    <Text>
                        EMAIL
                </Text>

                </Button>
                </View>
                )
            
        }
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>
                            Detail
        </Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={{ paddingHorizontal: 5 }} >
                    <Title style={{ color: 'red' }}>Book Title:  {this.state.data.name}</Title>
                    <Text center style={{ textAlign: 'center' }}>Author: {this.state.data.author}</Text>
                    <Text center style={{ textAlign: 'center' }}>Owner: {this.state.data.username}</Text>
                    <Image
                        style={{
                            paddingVertical: 30,
                            width: 150,
                            height: 150,
                            borderRadius: 20,
                            alignSelf: 'center'
                        }}
                        resizeMode='cover'
                        source={{
                            uri: this.state.data.image
                        }}
                    />
                    <Card>
                        <CardItem>
                            <Body>
                                <Text>Description: {this.state.data.description}</Text>
                            </Body>
                        </CardItem>
                    </Card>

                </Content>
                {this.renderBtn()}
                
            </Container>
        );
    }
}



export default Detail;
