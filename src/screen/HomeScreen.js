import React from 'react';
import { StatusBar, Image, TouchableOpacity, FlatList, Dimensions ,BackHandler,ToastAndroid } from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import ActionButton from 'react-native-action-button';
import firebase from "react-native-firebase";
import Collapsible from 'react-native-collapsible';
import { Container, Header, Title, Left, Icon, Right, Button, Body, ListItem, Thumbnail, Content, Text, Card, CardItem, View, Item, Input } from 'native-base';
import MyFooter from '../components/MyFooter'

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class HomeScreen extends React.Component {


handleBackButton() {
    ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
    return true;
}

  static navigationOptions = ({ navigation }) => ({
  });

  handlerNav = (e) => this.props.navigation.navigate(e)

  componentWillMount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    this.resetLocation()
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.unsubscribe = this.ref.onSnapshot((query) => {
      const tArray = []

      query.forEach((item) => {
        var temp = item.data()
        if (temp.image) {
          temp.image = 'data:image/png;base64,' + temp.image
        }

        tArray.push(temp)

      })
      console.log(JSON.stringify(tArray))
      this.setState({
        isLoadedData: true,
        markers: tArray,
        pets: tArray
      })
    })

  }

  constructor(props) {
    super(props);
    this.handlerNav = this.handlerNav.bind(this)
    this.state = {
      region: {
        latitude: 10.842363,
        longitude: 106.730517,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      herePos: {
        latitude: 10.842363,
        longitude: 106.730517,
      },
      isLoaded: false,
      isLoadedData: false,
      markers: [],
      pets: [],
      searchCollapsed: true
    }
    this.ref = firebase.firestore().collection('Book');
  }
  resetLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      if ((position.coords.latitude) && (position.coords.longitude)) {
     
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          },
          herePos: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          },
          isLoaded: true,
        });
      }

    }, (error) => {
      this.setState({
        isLoaded: true,
        
      });
      alert(JSON.stringify(error))
    }, {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      });

  }


 
  setPos(pos) {
    this.setState({
      herePos: pos
    })
  }
  viewDetail(marker) {
    this.props.navigation.navigate('Detail', {
      data: marker
    })
  }
  filterSearch(text) {
    newData = this.state.markers
    newData = newData.filter((item) => {
      const itemData = item.name.toUpperCase()
      const textData = text.toUpperCase()
      return itemData.indexOf(textData) > -1
    });
    this.setState({
      text: text,
      pets: newData // after filter we are setting users to new array
    });
  }

  setRegion(location) {
   
    this.setState({
      region: {
        latitude: location.latitude,
        longitude: location.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }
    })
  }

  renderPet = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => { this.setRegion(item.location) }}>
        <ListItem onPress={() => { this.setRegion(item.location) }}>
          <Thumbnail square size={60} source={{ uri: item.image }} />
          <Body>
            <Text>{item.name}</Text>
            <Text note>{item.username}</Text>
          </Body>
        </ListItem>
      </TouchableOpacity>
    )
  }
  renderList() {
    if (!this.state.searchCollapsed) {
      return (
        <View style={{ height: 500, elevation: 4, zIndex: 4, backgroundColor: 'white' }} align="center">

          <FlatList data={this.state.pets}
            renderItem={this.renderPet}
            keyExtractor={(item, index) => index}
            style={{ margin: 0, padding: 0 }} />

        </View>
      )
    }
  }
  searchExpand() {
    if (!this.state.searchCollapsed) {
      const a = ""
      this.setState({
        text: null
      });
      this.filterSearch(a)
    }
    this.setState({
      searchCollapsed: !this.state.searchCollapsed
    });
  }

  render() {
    return (
      <Container>
        <Collapsible collapsed={!this.state.searchCollapsed} align="center">
          <Header>
            <Left>
              <Button transparent onPress={() => this.props.navigation.navigate('DrawerOpen')}>
                <Icon name="menu" />
              </Button>
            </Left>
            <Body>
              <Title>
                Find books
                </Title>
            </Body>
            <Right>
                          
              <Button transparent onPress={() => this.searchExpand()} >
                <Icon name="search" />
              </Button>
            </Right>
          </Header>
        </Collapsible>
        <Collapsible collapsed={this.state.searchCollapsed} align="center">
          <Header searchBar rounded>
            <Item>
              <Icon active name="search" />
              <Input onChangeText={(text) => this.filterSearch(text)}
                value={this.state.text} placeholder="Search" />
              <Icon active name="close" onPress={() => this.searchExpand()} />
            </Item>
            <Button transparent>
              <Text>Search</Text>
            </Button>
          </Header>
        </Collapsible>
      
        {
          this.renderList()
        }
        {!(this.state.isLoaded && this.state.isLoadedData) &&
          <Content>
            <Text>Loading</Text>
          </Content>
        }
        {(this.state.isLoaded && this.state.isLoadedData) &&
        
          <MapView
             provider={PROVIDER_GOOGLE}
            style={{  zIndex: -1, height: '100%', width: '100%' }}
            initialRegion={this.state.region}
            showsUserLocation={true}
            onPress={(event) => this.setPos(event.nativeEvent.coordinate)}
            //onRegionChange={ region => this.setState({region}) }
            //onRegionChangeComplete={ region => this.setState({region}) }
          >

         

           

            {this.state.markers.map((marker, index) =>
              (
                <MapView.Marker
                  coordinate={marker.location}
                  onPress={() => { this.viewDetail(marker) }}
                  key={index}
                >
                  <TouchableOpacity onPress={() => { this.viewDetail(marker) }}>
                  {(marker.image.length >2) &&
                    <Image style={{ width: 45, height: 45, borderWidth: 1, borderRadius: 5, borderColor: 'red' }} source={{ uri: marker.image }} />
                  }
                    <View style={{ width: 45, alignItems: 'center', backgroundColor: 'rgba(255,255,255,0.6)' }}>
                      <Text style={{ fontSize: 8, fontWeight: 'bold', textAlign: 'center' }}>{marker.name} </Text>
                    </View>
                  </TouchableOpacity>
                </MapView.Marker>

              )

            )}
           

          </MapView>
      
        }
        
          <MyFooter  handlerNav={this.handlerNav} curActive={2}/>
      </Container>
    );
  }
}



export default HomeScreen;
