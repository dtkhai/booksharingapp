
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, ImageBackground, Dimensions } from 'react-native';
import { Button, Icon, Header, CheckBox } from 'native-base';
import LoginForm from '../components/LoginForm';

const { width, height } = Dimensions.get('window');

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handlerLogin = this.handlerLogin.bind(this)
  }
  handlerLogin = () => this.props.navigation.navigate('AllBooks')

  render() {

    return (
      <ImageBackground style={styles.container} source={require('../assets/img/HomeBackground1.jpg')}>

          <View style={styles.titleContainer}>
           
          </View>
          <View style={styles.box}  >
           
                <LoginForm handlerLogin={this.handlerLogin}/>
            
          </View>
    </ImageBackground>

        );
      }
    }
    
const styles = StyleSheet.create( {
          container: {
          flex: 1,
   
  
        alignItems: 'center',
        justifyContent: 'center',
      },
  titleContainer: {
         
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    
        
      },
  logo: {
          width: 70,
        height: 70
      },
  title: {
    alignSelf: 'center',
          fontSize: 30,
        fontWeight: 'bold',
        color: '#fff',
        marginTop: 35,
        textAlign: 'center',
 
      },
  box: {
    flex: 1,
        justifyContent: 'flex-end',      
        alignSelf:'stretch',
        paddingHorizontal: 10,
        paddingBottom: 20
      },
  footer: {
          flex: 4
      }
    } );
    
    export default Login;
