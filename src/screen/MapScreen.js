import React from 'react';
import { StatusBar, Image, TouchableOpacity, FlatList, Dimensions ,BackHandler,ToastAndroid } from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import ActionButton from 'react-native-action-button';
import firebase from "react-native-firebase";
import Collapsible from 'react-native-collapsible';
import { Container, Header, Title, Left, Icon, Right, Button, Body, ListItem, Thumbnail, Content, Text, Card, CardItem, View, Item, Input } from 'native-base';
import MyFooter from '../components/MyFooter'
import Polyline from '@mapbox/polyline';
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

@inject('userStore')
@observer
class MapScreen extends React.Component {



  static navigationOptions = ({ navigation }) => ({
  });



  componentWillMount() {
   
    this.resetLocation()
  }

  componentDidMount() {
      var fromString = this.state.herePos.latitude+","+this.state.herePos.longitude
      var desString = this.state.des.location.latitude+","+this.state.des.location.longitude
      console.log("TOA DO:",desString)
    this.getDirections(fromString, desString)
  }

  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.state = {
      region: {
        latitude: 10.842363,
        longitude: 106.730517,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      herePos: {
        latitude: 10.842363,
        longitude: 106.730517,
      },
      isLoaded: false,
      markers: [],
      coords:[],
      des: params ? params.des : null,

    }
  }

  async getDirections(startLoc, destinationLoc) {
    try {
        let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }`)
        let respJson = await resp.json();
        console.log("CAI GI DAY", JSON.stringify(respJson))
        let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
        let coords = points.map((point, index) => {
            return  {
                latitude : point[0],
                longitude : point[1]
            }
        })
        this.setState({coords: coords})
        console.log("TRA VE:", JSON.stringify(this.state.coords))
        return coords
    } catch(error) {
        console.log("LOI:", error)
        return error
    }
}

  resetLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
     
        this.setRegion(position.coords)


    }, (error) => {
      this.setState({
        isLoaded: true,
        
      });
      alert(JSON.stringify(error))
    }, {
        enableHighAccuracy: false,
        timeout: 5000,
        maximumAge: 0
      });

  }


 
  setPos(pos) {
    this.setState({
      herePos: pos
    })
  }

  viewDetail(marker) {
    this.props.navigation.navigate('Detail', {
      data: marker
    })
  }

  findDir(){
    var fromString = this.state.herePos.latitude+","+this.state.herePos.longitude
    var desString = this.state.des.location.latitude+","+this.state.des.location.longitude
    console.log("TOA DO:",desString)
  this.getDirections(fromString, desString)
  }

  setRegion(location) {
    this.setState({
      region: {
        latitude: location.latitude,
        longitude: location.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      herePos: {
        latitude: location.latitude,
        longitude: location.longitude,
      },
      isLoaded: true,
    },()=>{
        this.findDir()
    })
  }



  render() {
    const {des} = this.state
    return (
      <Container>

          <Header>
          <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
            <Body>
              <Title>
                Find books
                </Title>
            </Body>
          <Right>
              </Right>
          </Header>

     
        {!(this.state.isLoaded) &&
          <Content>
            <Text>Loading</Text>
          </Content>
        }
        {(this.state.isLoaded) &&
        
          <MapView
             provider={PROVIDER_GOOGLE}
            style={{  zIndex: -1, height: '100%', width: '100%' }}
            initialRegion={this.state.region}
            showsUserLocation={true}
            onPress={(event) => this.setPos(event.nativeEvent.coordinate)}
            //onRegionChange={ region => this.setState({region}) }
            //onRegionChangeComplete={ region => this.setState({region}) }
          >
             <MapView.Polyline 
          coordinates={this.state.coords}
          strokeWidth={4}
          strokeColor="red"/>
 
         

           

          <MapView.Marker
                  coordinate={des.location}
                  onPress={() => { this.viewDetail(des) }}
                
                >
                  <TouchableOpacity onPress={() => { this.viewDetail(des) }}>
                
                    <Image style={{ width: 45, height: 45, borderWidth: 1, borderRadius: 5, borderColor: 'red' }} source={{ uri: des.image }} />
                  
                    <View style={{ width: 45, alignItems: 'center', backgroundColor: 'rgba(255,255,255,0.6)' }}>
                      <Text style={{ fontSize: 8, fontWeight: 'bold', textAlign: 'center' }}>{des.name} </Text>
                    </View>
                  </TouchableOpacity>
                </MapView.Marker>
           

          </MapView>
      
        }
        
      </Container>
    );
  }
}



export default MapScreen;
