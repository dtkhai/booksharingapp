import React from 'react';
import { StatusBar, Alert, Image } from 'react-native';
import MapView from 'react-native-maps';
import { Container,Picker, Header, Title, Left, Icon, Right, Button, Form, Item, Label, Input, Body, Content, Text, Card, CardItem, View } from 'native-base';
import firebase from "react-native-firebase";
import PhotoUpload from 'react-native-photo-upload'

import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";
@inject('userStore')
@observer
class NewPost extends React.Component {
    static navigationOptions = ({ navigation }) => ({
    });

    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            pos: params ? params.pos : null,
            image: params ? params.image : null,
            name: "",
            author: "",
            phone: '',
            description: '',
            tag: "General"
        }
        this.ref = firebase.firestore().collection('Book');
        this.storageRef = firebase.storage().ref('image');
    }
    postNew() {
        
        var tempID = JSON.stringify(this.state.pos.latitude + this.state.pos.longitude) + JSON.stringify(Math.floor((Math.random() * 10000) + 1));
        var tdata = {
            name: this.state.name,
            username: this.props.userStore.user.displayName,
            email: this.props.userStore.user.email,
            author: this.state.author,
            description: this.state.description,
            location: this.state.pos,
            tag: this.state.tag
        }
        
        if(this.state.image){
            tdata.image = this.state.image
        }

        this.ref.doc(tempID).set(tdata).then((data) => {
            Alert.alert('Successfull')
            this.props.navigation.goBack()
        })
    }

    onValueChange(value) {
        this.setState({
          tag: value
        });
      }

    componentDidMount() {
       
    }
    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>
                            New Book
        </Title>
                    </Body>
                    <Right />
                </Header>
                <Content>
                    <Title style={{ color: 'red' }}>User:  {this.props.userStore.user.displayName}</Title>
                    <Image
                            style={{
                                paddingVertical: 30,
                                width: 150,
                                height: 150,
                                borderRadius: 0,
                                alignSelf: 'center'
                            }}
                            resizeMode='cover'
                            source={{
                                uri:  this.state.image
                            }}
                        />

                    <Form>
                        <Item stackedLabel>
                            <Label>Book Title</Label>
                            <Input onChangeText={(text) => { this.setState({ name: text }) }} />
                        </Item>
                        <Item stackedLabel>
                            <Label>Author</Label>
                            <Input onChangeText={(text) => { this.setState({ author: text }) }} />
                        </Item>
                        <Item stackedLabel>
                            <Label>Description</Label>
                            <Input onChangeText={(text) => { this.setState({ description: text }) }} />
                        </Item>
                        <Item>
                        <Label>Categorys</Label>
                        <Picker
              mode="dropdown"
              iosIcon={<Icon name="ios-arrow-down-outline" />}
              placeholder="Select your SIM"
              placeholderStyle={{ color: "#bfc6ea" }}
              placeholderIconColor="#007aff"
              style={{ width: undefined }}
              selectedValue={this.state.tag}
              onValueChange={this.onValueChange.bind(this)}
            >
              <Picker.Item label="General" value="General" />
              <Picker.Item label="Economy" value="Economy" />             
              <Picker.Item label="Romantic" value="Romantic" />
              <Picker.Item label="Science" value="Science" />
              <Picker.Item label="Bibiography" value="Bibiography" />
              <Picker.Item label="Manga" value="Manga" />
              <Picker.Item label="History" value="History" />
              <Picker.Item label="Fiction" value="Fiction" />
              <Picker.Item label="Other" value="Other" />
            </Picker>
                            </Item>
                    </Form>
                  
                  
                </Content>
                <Button full rounded success style={{height:50,margin:5}} onPress={() => this.postNew()}>
            <Text style={{color: "white", fontWeight: "bold"}}>Post</Text>
          </Button>
            </Container>
        );
    }
}



export default NewPost;
