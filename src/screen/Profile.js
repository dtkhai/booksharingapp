import React from 'react';
import { StatusBar, Alert, Image , AsyncStorage,BackHandler,ToastAndroid} from 'react-native';
import MapView from 'react-native-maps';
import { Container, Header, Title, Left, Icon, Right, Button, Form, Item, Label, Input, Body, Content, Text, Card, CardItem, View } from 'native-base';
import firebase from "react-native-firebase";
import Communications from 'react-native-communications';
import MyFooter from '../components/MyFooter'
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";
import { GoogleSignin } from 'react-native-google-signin';

GoogleSignin.configure({
 
});

var STORAGE_KEY = 'id_token';

@inject('userStore')
@observer
class Profile extends React.Component {
    static navigationOptions = ({ navigation }) => ({
    });
    
    constructor(props) {
        super(props);
        this.handlerNav = this.handlerNav.bind(this)
        const { params } = this.props.navigation.state;
        this.state = {
            data: params ? params.data : null,
        } 
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }

    async _userLogout() {
        try {
          var tokenDataString = await AsyncStorage.getItem(STORAGE_KEY);
          var tokenData = JSON.parse(tokenDataString);
          if(tokenData.method=="google"){
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
          }
          await AsyncStorage.removeItem(STORAGE_KEY);
         
          
          this.props.navigation.navigate("Login")
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
      }

    
  
    handlerNav = (e) => this.props.navigation.navigate(e)
    render() {
        return (
            <Container>
               
                <Content style={{ paddingHorizontal: 5,paddingTop:10, }} >
                <Image
                            style={{
                                paddingVertical: 30,
                                width: 150,
                                height: 150,
                                borderRadius: 75,
                                alignSelf: 'center'
                            }}
                            resizeMode='cover'
                            source={{
                                uri:  this.props.userStore.user.photoURL
                            }}
                        />
                    <Title style={{ color: 'red' }}>{this.props.userStore.user.displayName}</Title>
                    <Text center style={{ textAlign: 'center' }}>{this.props.userStore.user.email}</Text>
                    <Button full rounded light style={{height:50,margin:20}} onPress={() => this._userLogout()}>
            <Text style={{color: "#ea5435", fontWeight: "bold"}}>Log out</Text>
          </Button>
                            
                </Content>
                <MyFooter  handlerNav={this.handlerNav} curActive={1}/>
            </Container>
        );
    }
}



export default Profile;
