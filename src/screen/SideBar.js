var STORAGE_KEY = 'id_token';
import React from 'react';
import { View, AppRegistry, Image, StatusBar, StyleSheet,AsyncStorage } from 'react-native';
import { Container, Content, Text, List, ListItem, Left, Right, Thumbnail, Icon } from 'native-base';
import { GoogleSignin } from 'react-native-google-signin';

GoogleSignin.configure({
 
});

const data = [
  {
    name: 'Home',
    route: 'HomeScreen',
    icon: 'home'
  },
  {
    name: 'Your Book',
    route: 'YourPets',
    icon: 'md-paw'
  },
];

import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";



@inject('userStore')
@observer
export default class SideBar extends React.Component {
  async _userLogout() {
    try {
      var tokenDataString = await AsyncStorage.getItem(STORAGE_KEY);
      var tokenData = JSON.parse(tokenDataString);
      if(tokenData.method=="google"){
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }
      await AsyncStorage.removeItem(STORAGE_KEY);
     
      
      this.props.navigation.navigate("Login")
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }


  render() {
    return (
      <View>

        <View style={{

          height: 150,
          alignSelf: 'stretch',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#3e5fad'
        }}>
          <View style={{
            height: 70, width: 70, borderRadius: 35, backgroundColor: 'white', justifyContent: 'center',
            alignItems: 'center'
          }}>
            <Image  source={{
                                uri: this.props.userStore.user.photoURL
                            }} style={{ height: 60, width: 60, borderRadius: 100 }} />
          </View>
          <View style={{ marginLeft: 10 }}>
            <Text style={{ color: 'white' }}>
              {this.props.userStore.user.displayName}
            </Text>
            <Text >
              Member
            </Text>
          </View>
        </View>
        <List dataArray={data} renderRow={data => {
          return (
            <ListItem button onPress={() => this.props.navigation.navigate(data.route)}>
              <Icon
                active
                name={data.icon}
                style={{ color: "#777", fontSize: 26, width: 30 }}
              />
              <Text style={styles.text}>
                {data.name}
              </Text>
            </ListItem>
          );
        }} />
        <ListItem button onPress={() => this._userLogout()}>
          <Icon
            active
            name="md-log-out"
            style={{ color: "#777", fontSize: 26, width: 30 }}
          />
          <Text style={styles.text}>
            Log out
                  </Text>
        </ListItem>
      </View>
    );
  }
}


const styles = StyleSheet.create({

  text: {
    fontWeight: "400",
    fontSize: 16,
    marginLeft: 20
  },
  badgeText: {
    fontSize: 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: -3
  }
}
)