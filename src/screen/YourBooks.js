import React from 'react';
import { StatusBar, Alert, Image, FlatList, TouchableOpacity, BackHandler, ToastAndroid } from 'react-native';
import MapView from 'react-native-maps';
import { Container, ActionSheet, Header, Title, Left, Icon, Right, Button, ListItem, Thumbnail, Form, Item, Label, Input, Body, Content, Text, Card, CardItem, View } from 'native-base';
import firebase from "react-native-firebase";
import Communications from 'react-native-communications';
import ActionButton from 'react-native-action-button';
import MyFooter from '../components/MyFooter'
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";
var ImagePicker = require('react-native-image-picker');

var BUTTONS = [
    { text: "Camera", icon: "camera", iconColor: "#2c8ef4" },
    { text: "Galery", icon: "md-image", iconColor: "#f42ced" },
    { text: "Cancel", icon: "close", iconColor: "#25de5b" }
];

var options = {
    title: 'Select Image',
    compressImageMaxWidth: 400,
    compressImageQuality: 0.5,
    customButtons: [
        { name: 'fb', title: 'Choose Photo from Facebook' },
    ],
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

var CANCEL_INDEX = 3;

const DATA = [
    {
        name: "Hay",
        author: "Steve JObs",
        image: "https://pictures.abebooks.com/ARMYDOC/20595056776.jpg",
        username: "KHai",
        description: "This book is about Khai",
        location: {
            latitude: 10.7750247,
            longitude: 106.6978738,
        }
    },
    {
        name: "Ngu",
        author: "Bill GAte JObs",
        image: "https://images-na.ssl-images-amazon.com/images/I/A1HI7mXRymL.jpg",
        username: "No",
        description: "This book is goof",
        location: {
            latitude: 10.762546,
            longitude: 106.680989,
        }
    },
    {
        name: "Best",
        author: "Trump",
        image: "https://upload.wikimedia.org/wikipedia/en/thumb/1/1c/Trump_the_art_of_the_deal.jpg/220px-Trump_the_art_of_the_deal.jpg",
        username: "Trump",
        description: "Make american graete again",
        location: {
            latitude: 10.8454029,
            longitude: 106.7261562,
        }
    },

]

@inject('userStore')
@observer
class YourBooks extends React.Component {
    static navigationOptions = ({ navigation }) => ({
    });

    handlerNav = (e) => this.props.navigation.navigate(e)

    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            data: params ? params.data : null,
            listBook : [],
            listFiltered: [],
            books: [],
            herePos: {
                latitude: 10.842363,
                longitude: 106.730517,
            }
        }
        this.ref = firebase.firestore().collection('Book');

    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.unsubscribe = this.ref.onSnapshot((query) => {
            const tArray = []

            query.forEach((item) => {
                
                    var temp = item.data()
                   
                    if (temp.email == this.props.userStore.user.email) {
                        tArray.push(temp)
                    }
                
            })
            console.log(JSON.stringify(tArray))
            this.setState({

                listBook: tArray,
                listFiltered: tArray
            })
        })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }

    viewDetail(marker) {
        this.props.navigation.navigate('Detail', {
            data: marker
        })
    }
    renderPet = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => { this.viewDetail(item) }}>
                <ListItem onPress={() => { this.viewDetail(item) }}>
                    <Thumbnail square size={60} source={{ uri: item.image }} />
                    <Body>
                        <Text>{item.name}</Text>
                        <Text note>{item.author}</Text>
                        <Text note>{item.description}</Text>

                    </Body>
                </ListItem>
            </TouchableOpacity>
        )
    }

    filterSearch(text) {
        newData = this.state.listBook
        newData = newData.filter((item) => {
            const itemData = item.name.toUpperCase()
            const textData = text.toUpperCase()
            return itemData.indexOf(textData) > -1
        });
        this.setState({
            text: text,
            listFiltered: newData // after filter we are setting users to new array
        });
    }

    createNew() {
        this.props.navigation.navigate('New', {
            pos: this.props.userStore.getLoc
        })
    }

    methodSelect(buttonIndex) {
        if (buttonIndex == 0) {
            this.props.navigation.navigate('Camera')
        } else if (buttonIndex == 1) {
            ImagePicker.launchImageLibrary(options, (response) => {
                console.log('Response = ', response);

                if (response.didCancel) {
                    console.log('User cancelled image picker');
                }
                else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                }
                else if (response.customButton) {
                    console.log('User tapped custom button: ', response.customButton);
                }
                else {
                    //let source =  response.path ;

                    // You can also display the image using data:
                     let source =   'data:image/jpeg;base64,' + response.data ;
                    console.log('hinh day:' + source)
                    this.props.navigation.navigate('New', {
                        image: source,
                        pos: {
                            latitude: this.props.userStore.curLoc.latitude,
                            longitude: this.props.userStore.curLoc.longitude
                          }
                    })
                 
                }
            });
        }
    }

    render() {
        return (
            <Container>
                <Header searchBar rounded>
                    <Item>
                        <Icon active name="search" />
                        <Input onChangeText={(text) => this.filterSearch(text)}
                            value={this.state.text} placeholder="Search" />

                    </Item>
                    <Button transparent>
                        <Text>Search</Text>
                    </Button>
                </Header>
                <Content style={{ paddingHorizontal: 5 }} >
                    <FlatList data={this.state.listFiltered}
                        renderItem={this.renderPet}
                        keyExtractor={(item, index) => index}
                        style={{ margin: 0, padding: 0 }} />

                </Content>
                <ActionButton
                    size={55}
                    offsetY={60}
                    buttonColor="rgba(231,76,60,1)"
                    onPress={() => {
                        ActionSheet.show(
                            {
                                options: BUTTONS,
                                cancelButtonIndex: CANCEL_INDEX,
                                title: "Choose"
                            },
                            buttonIndex => {
                                this.methodSelect(buttonIndex);
                            }
                        )
                    }}
                />
                <MyFooter handlerNav={this.handlerNav} curActive={3} />

            </Container>
        );
    }
}



export default YourBooks;
