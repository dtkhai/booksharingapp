import React from 'react';
import { StatusBar, Alert, Image, FlatList, TouchableOpacity } from 'react-native';
import MapView from 'react-native-maps';
import { Container, Header, Title, Left, Icon, Right, Button,ListItem,Thumbnail, Form, Item, Label, Input, Body, Content, Text, Card, CardItem, View } from 'native-base';
import firebase from "react-native-firebase";


import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";
@inject('userStore')
@observer
class YourPets extends React.Component {
    static navigationOptions = ({ navigation }) => ({
    });

    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            data: params ? params.data : null,
            pets: []
        }
        this.ref = firebase.firestore().collection('Pet');

    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot((query) => {
            const tArray = []

            query.forEach((item) => {
                if (this.props.userStore.user.name != 'Anonymous') {
                    var temp = item.data()
                    if (temp.image) {
                        temp.image = 'data:image/png;base64,' + temp.image
                    }
                    if (temp.username == this.props.userStore.user.name) {
                        tArray.push(temp)
                    }
                }
            })
            console.log(JSON.stringify(tArray))
            this.setState({
                isLoadedData: true,
                pets: tArray
            })
        })

    }

    viewDetail(marker) {
        this.props.navigation.navigate('Detail', {
            data: marker
        })
    }
    renderPet = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => { this.viewDetail(item) }}>
                <ListItem onPress={() => { this.viewDetail(item) }}>
                    <Thumbnail square size={60} source={{ uri: item.image }} />
                    <Body>
                        <Text>{item.name}</Text>
                        <Text note>{item.username}</Text>
                    </Body>
                </ListItem>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.navigate('DrawerOpen')}>
                            <Icon name="menu" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>
                            Your Books
                        </Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={{ paddingHorizontal: 5 }} >
                    <FlatList data={this.state.pets}
                        renderItem={this.renderPet}
                        keyExtractor={(item, index) => index}
                        style={{ margin: 0, padding: 0 }} />

                </Content>


            </Container>
        );
    }
}



export default YourPets;
