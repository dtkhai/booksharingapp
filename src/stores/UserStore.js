import { observable, action, computed } from "mobx";
import { ToastAndroid ,Alert} from 'react-native'


export default class UserStore {
    // attributes
    @observable user = {
        'email' : '',
        'displayName': '',
        "photoURL": "",
        "uid": ""
    }

    @observable curLoc ={
        "latitude": 10.842363,
        "longitude": 106.730517
    }

    // reset
    @action reset() {
        this.user = {
            'email' : '',
            'displayName': '',
            "photoURL": "",
            "uid": ""
        }
    }
    // action/method
    @action StoreUser(user) {
        this.user = {
            'email' : user.email,
            'displayName': user.displayName,
            "photoURL": user.photoURL,
            "uid": user.uid
        }
    }

    @action setLoc(la, long) {
        this.curLoc = {
            "latitude": la,
            "longitude": long
        }
        //alert("TOA DO" + JSON.stringify(this.curLoc))
    }

    @computed get getLoc() {
        return this.curLoc;
    }

    @computed get GetUser() {
        return this.user;
    }
 
}

